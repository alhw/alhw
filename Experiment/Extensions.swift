//
//  Extensions.swift
//  Experiment
//
//

import UIKit

// I'm cracking this out pretty quickly and the naming is meh
extension Date {
    static let iso8601Formatter: ISO8601DateFormatter = {
        let formatter = ISO8601DateFormatter()
        formatter.formatOptions = [.withFullDate,
                                          .withTime,
                                          .withDashSeparatorInDate,
                                          .withColonSeparatorInTime]
        return formatter
    }()
    static let labelFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .full
        return formatter
    }()
}

extension String {
    static func fromDate(date: String) -> String {
        let date = Date.iso8601Formatter.date(from: date)
        guard let unwrappedDate = date else {
            return ""
        }
        return Date.labelFormatter.string(from: unwrappedDate)
    }
}
