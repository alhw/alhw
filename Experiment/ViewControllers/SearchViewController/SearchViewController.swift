//
//  SearchViewController.swift
//  Experiment
//
import UIKit
import Combine

fileprivate struct Constants {
    static let Title = "Search"
    static let SearchPlaceholder = "Search"
    static let DebounceMs: Int = 250
    static let DetailViewController = "DetailViewController"
    static let Storyboard = UIStoryboard(name: "Main", bundle: nil)
}

fileprivate enum Cells {
    case SearchResultCell
    var nibName: String {
        switch self {
        case .SearchResultCell:
            return "SearchResultCell"
        }
    }
    var reuseIdentifier: String {
        switch self {
        case .SearchResultCell:
            return "SearchResultCell"
        }
    }
}

class SearchViewController: UIViewController {
    let searchController = UISearchController(searchResultsController: nil)
    var cancellable = [AnyCancellable]()
    var selectedIndexPath: IndexPath?
    @IBOutlet weak var tableView: UITableView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = Constants.Title
        definesPresentationContext = true
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = Constants.SearchPlaceholder
        navigationItem.searchController = searchController
        tableView.register(
            UINib(nibName: Cells.SearchResultCell.nibName, bundle: nil),
            forCellReuseIdentifier: Cells.SearchResultCell.reuseIdentifier)
        pollSearchField()
    }
}

extension SearchViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ApplicationState.events.count;
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Cells.SearchResultCell.reuseIdentifier) as! SearchResultCell
        let event = ApplicationState.events[indexPath.row]
        cell.event = event
        cell.isFavorite = isEventFavorte(event: event)
        return cell
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
}

extension SearchViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndexPath = indexPath
        tableView.deselectRow(at: indexPath, animated: true)
        let detailViewController  = Constants.Storyboard.instantiateViewController(withIdentifier: Constants.DetailViewController) as! DetailViewController
        detailViewController.delegate = self
        navigationController?.pushViewController (detailViewController, animated: true)
    }
}

extension SearchViewController: SearchViewControllerDelegate {
    func selectedEvent() -> Event? {
        guard let indexPath = selectedIndexPath else {
            return nil
        }
        return ApplicationState.events[indexPath.row]
    }
    func setFavoriteForEvent(event: Event) {
        guard let id = event.id else {
            return
        }
        if (isEventFavorte(event: event)) {
            ApplicationState.favorites[id] = false
        } else {
            ApplicationState.favorites[id] = true
        }
    }
    func isEventFavorte(event: Event) -> Bool {
        guard let id = event.id else {
            return false
        }
        if let value = ApplicationState.favorites[id] {
            return value
        } else {
            return false
        }
    }
}

extension SearchViewController {
    func pollSearchField() {
          NotificationCenter.default.publisher(
            for: UISearchTextField.textDidChangeNotification,
            object: searchController.searchBar.searchTextField)
            .map {
              ($0.object as! UISearchTextField).text
            }
            .debounce(for: .milliseconds(Constants.DebounceMs), scheduler: RunLoop.main)
            .map({ value -> String in
                return value ?? ""
            })
            .sink(receiveValue: { (value) in
                DispatchQueue.global(qos: .userInteractive).async { [weak self] in
                    API.Events.search(query: value) { (success, events) in
                        ApplicationState.events = events;
                        DispatchQueue.main.async {
                            self?.tableView.reloadData()
                        }
                    }
            }
          })
          .store(in: &cancellable)
    }
}
