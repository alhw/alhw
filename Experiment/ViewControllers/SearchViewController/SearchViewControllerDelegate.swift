//
//  SearchViewControllerDelegate.swift
//  Experiment
//

protocol SearchViewControllerDelegate {
    func selectedEvent() -> Event?;
    func isEventFavorte(event: Event) -> Bool;
    func setFavoriteForEvent(event: Event);
}
