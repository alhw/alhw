//
//  DetailViewController.swift
//  Experiment
//

import UIKit

fileprivate struct Constants {
    static let EmptyField = "N\\A"
    static let imageFavorite = UIImage(systemName: "star.fill")
    static let imageNotFavorite = UIImage(systemName: "star")

}
class DetailViewController : UIViewController {
    public var delegate: SearchViewControllerDelegate?
    private var event: Event?
    @IBOutlet weak var buttonFavorite: UIBarButtonItem!
    @IBOutlet weak var header : UILabel!
    @IBOutlet weak var location : UILabel!
    @IBOutlet weak var date : UILabel!

    func refreshFavoriteStatus() {
        if let event = event, let delegate = delegate {
            buttonFavorite.image = delegate.isEventFavorte(event: event) ?
                Constants.imageFavorite :
                Constants.imageNotFavorite
        }
    }
    
    override  func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        buttonFavorite.image = Constants.imageFavorite
        if let delegate = delegate {
            event = delegate.selectedEvent()
        }
        title = event?.short_title ?? ""
        header.text = event?.short_title ?? Constants.EmptyField
        location.text = event?.venue?.display_location ?? Constants.EmptyField
        if let utcDate = event?.datetime_utc {
            date.text = String.fromDate(date: utcDate)
        } else {
            date.text = Constants.EmptyField
        }
        refreshFavoriteStatus()
    }
    
    @IBAction func tapFavorite(_ sender: UIBarButtonItem) {
        if let event = event {
            delegate?.setFavoriteForEvent(event: event)
        }
        refreshFavoriteStatus()
    }
}
