//
//  Event.swift
//  Experiment

public struct Venue: Codable {
    let name: String?
    let name_v2: String?
    let state: String?
    let address: String?
    let country: String?
    let url: String?
    let display_location: String?
}

public struct Performer: Codable {
    let type: String?
    let image: String?
}

public struct Event: Codable {
    let id: Int?
    let datetime_utc: String?
    let venue: Venue?
    let short_title: String?
    let type: String?
   let performers: [Performer]?
}

public struct EventResponse: Codable {
    let events: [Event]?
}
