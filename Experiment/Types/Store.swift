//
//  Store.swift
//  Experiment
import Foundation

public class State : Codable {
    public var events: [Event] = []
    public var favorites: [Int: Bool] = [:]
    // TODO: I was going to seralize this to userdefaults for cheap persistance but ran out of time
    func save() {
    }
    func restore() {
    }
    func reset() {
        events.removeAll()
    }
}

public var ApplicationState: State = State()
