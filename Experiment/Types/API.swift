//
//  API.swift
//  Experiment
//
//

import UIKit

public struct API {}
// Note: there's a lot more I wanted to do here but ran short on time, ideally I'd just places this inside the combine pipeline
//        I'm not a huge fan of reasoning about compleation handlers w/ combine forcing to much logic in sink()
extension API {
    static let BaseURL: String = "https://api.seatgeek.com"
    static let ClientId: String = "MjEzODk3MTd8MTYwNTE1MTA4OC4zNDY1MjQ3"
    public struct Events {
        static func search(query: String, onComplete: @escaping (Bool, [Event]) -> Void) {
            var components = URLComponents(string: "\(BaseURL)/2/events")
            components?.queryItems = [
               URLQueryItem(name: "client_id", value: ClientId),
               URLQueryItem(name: "q", value: query)
            ]
            guard let url = components?.url else {
                onComplete(false, [])
                return
            }
            URLSession.shared.dataTask(with: url, completionHandler: { data, response, error in
                do {
                    let json = try JSONDecoder().decode(EventResponse.self, from: data! )
                    try JSONSerialization.jsonObject(with: data!, options: [])
                    onComplete(true, json.events ?? [])
                } catch {
                    onComplete(false, [])
                }

            }).resume()
        }
    }
}
