//
//  SearchResultCell.swift
//  Experiment
//
//

import UIKit

fileprivate struct Constants {
    static let EmptyField = "N\\A"
    static let imageFavorite = UIImage(systemName: "star.fill")
    static let imageNotFavorite = UIImage(systemName: "star")
}

class SearchResultCell : UITableViewCell {
    @IBOutlet weak var favoriteImage : UIImageView!
    @IBOutlet weak var title : UILabel!
    @IBOutlet weak var location : UILabel!
    @IBOutlet weak var date : UILabel!
    var isFavorite : Bool? {
        didSet {
            favoriteImage.image = isFavorite! ? Constants.imageFavorite : Constants.imageNotFavorite
        }
    }
    var event : Event? {
        didSet {
            title.text = event?.short_title ?? Constants.EmptyField
            location.text = event?.venue?.display_location ?? Constants.EmptyField
            if let utcDate = event?.datetime_utc {
                date.text = String.fromDate(date: utcDate)
            } else {
                date.text = Constants.EmptyField
            }
            layoutIfNeeded()
        }
    }
}
