# Experiment

## Build & Run

This app only uses Apple provided frameworks so no external dependancy managment tooling is required. 
simply clicking play in Xcode (v. 12.2) should launch the project in your simulator

## Testing

Omitted due to time constraints (time-boxed myself to ~2.5 hrs) I did keep state isolated and dependancy free to allow for easier unit tests.
